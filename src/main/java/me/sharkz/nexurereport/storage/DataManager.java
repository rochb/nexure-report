package me.sharkz.nexurereport.storage;

import me.sharkz.nexurereport.NR;
import me.sharkz.nexurereport.loader.CredentialsLoader;
import me.sharkz.nexurereport.logger.SharkzLogger;
import me.sharkz.nexurereport.report.Report;
import me.sharkz.nexurereport.utils.sql.Column;
import me.sharkz.nexurereport.utils.sql.DataTypes;
import me.sharkz.nexurereport.utils.sql.QueryBuilder;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class DataManager {

    private final SDatabase database;
    private final FileConfiguration config;
    private final NR plugin;

    private String REPORTS_TABLE;

    public DataManager(NR plugin) {
        this.plugin = plugin;
        this.config = plugin.getConfig();
        DatabaseType type = DatabaseType.valueOf(config.getString("storage.type", "SQLITE"));
        if (!config.isConfigurationSection("storage")
                || !config.contains("storage.type")
                || type.getUrl() == null) {
            database = null;
            SharkzLogger.error("Invalid storage configuration !");
            SharkzLogger.info("Disabling plugin...");
            Bukkit.getPluginManager().disablePlugin(plugin);
            return;
        }
        DatabaseCredentials credentials = new CredentialsLoader().load(config.getConfigurationSection("storage"));
        this.database = new SDatabase(plugin, type, credentials);
        database.connect(connection -> {
            REPORTS_TABLE = credentials.getPrefix() + "reports";
            createTables();
        });
    }

    private void createTables() {
        try {
            query(new QueryBuilder(REPORTS_TABLE).createTableIfNotExists()
                    .column(Column.dataType("id", DataTypes.Limit.VARCHAR, 100))
                    .column(Column.dataType("player", DataTypes.Limit.VARCHAR, 100))
                    .column(Column.dataType("target", DataTypes.Limit.VARCHAR, 100))
                    .column(Column.dataType("content", DataTypes.TEXT))
                    .column(Column.dataType("state", DataTypes.INT))
                    .column(Column.dataType("createdAt", DataTypes.TIMESTAMP))
                    .primaryKey("id")
                    .build());
        } catch (SQLException throwables) {
            SharkzLogger.warn("Cannot create tables, please check your configuration.");
            throwables.printStackTrace();
        }
    }

    public void insert(Report report) {
        asyncQuery(new QueryBuilder(REPORTS_TABLE).insert()
                .insert("id", report.getId().toString())
                .insert("player", report.getPlayer().getUniqueId().toString())
                .insert("target", report.getTarget().getUniqueId().toString())
                .insert("content", report.getContent().replace("'", "’"))
                .insert("state", report.getState().getId())
                .insert("createdAt", new Timestamp(report.getDate().getTime()).toString())
                .build());
    }

    public void update(Report report) {
        asyncQuery(new QueryBuilder(REPORTS_TABLE).update()
                .set("state", report.getState().getId())
                .toWhere()
                .where("id", report.getId().toString())
                .build());
    }

    public void delete(Report report) {
        asyncQuery(new QueryBuilder(REPORTS_TABLE).delete()
                .where("id", report.getId().toString())
                .build());
    }

    public List<Report> getAll() {
        List<Report> reports = new ArrayList<>();
        try {
            ResultSet rs = executeQuery(new QueryBuilder(REPORTS_TABLE).select().buildAllColumns());
            while (rs.next())
                Report.State.getById(rs.getInt("state"))
                        .ifPresent(state -> {
                            try {
                                reports.add(new Report(UUID.fromString(rs.getString("id")),
                                        state,
                                        Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("player"))),
                                        Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("target"))),
                                        rs.getString("content").replace("’", "'"),
                                        new Date(rs.getTimestamp("createdAt").getTime())));
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            }
                        });
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reports;
    }

    private ResultSet executeQuery(String command) throws SQLException {
        return getConnection().prepareStatement(command).executeQuery();
    }

    private void query(String command) throws SQLException {
        getConnection().prepareStatement(command).executeUpdate();
    }

    private void asyncQuery(String command) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    query(command);
                } catch (SQLException throwable) {
                    SharkzLogger.warn("Error while performing command : " + throwable.getMessage());
                    throwable.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    private Connection getConnection() {
        return database.getConnection();
    }

    public SDatabase getDatabase() {
        return database;
    }
}
