package me.sharkz.nexurereport.storage;

import me.sharkz.nexurereport.NR;

import java.sql.Connection;

public interface IDatabase {

    DatabaseType getType();

    DatabaseCredentials getCredentials();

    boolean isLoaded();

    void closeConnection();

    void connect(ConnectionCall call);

    Connection createConnection(NR plugin, DatabaseType type);

    interface ConnectionCall {

        void onConnect(Connection connection);

    }
}
