package me.sharkz.nexurereport.storage;

public enum DatabaseType {
    MYSQL("com.mysql.jdbc.Driver", "jdbc:mysql://"),
    SQLITE("org.sqlite.JDBC", "jdbc:sqlite:");

    private final String driver;
    private final String url;

    DatabaseType(String driver, String url) {
        this.driver = driver;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getDriver() {
        return driver;
    }
}
