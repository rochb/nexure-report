package me.sharkz.nexurereport.storage;

import me.sharkz.nexurereport.NR;
import me.sharkz.nexurereport.logger.SharkzLogger;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SDatabase implements IDatabase {

    private final DatabaseType type;
    private final DatabaseCredentials credentials;
    private final Connection connection;
    private final NR plugin;
    private boolean loaded;


    public SDatabase(NR plugin, DatabaseType type, DatabaseCredentials credentials) {
        this.type = type;
        this.plugin = plugin;
        this.credentials = credentials;
        this.connection = createConnection(plugin, type);
    }

    @Override
    public DatabaseType getType() {
        return type;
    }

    @Override
    public DatabaseCredentials getCredentials() {
        return credentials;
    }

    @Override
    public boolean isLoaded() {
        return loaded;
    }

    @Override
    public void closeConnection() {
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException throwables) {
            SharkzLogger.error("Cannot close database connection : ");
            throwables.printStackTrace();
        }
    }

    @Override
    public void connect(ConnectionCall call) {
        if (connection == null) {
            try {
                this.createConnection(plugin, type);
            } catch (Exception ex) {
                SharkzLogger.error("&cCannot connect to database :");
                ex.printStackTrace();
            }
        }

        try {
            call.onConnect(connection);
        } catch (Exception ex) {
            SharkzLogger.error("&cCannot execute " + type.name() + " connection call:");
            ex.printStackTrace();
        }
    }

    @Override
    public Connection createConnection(NR plugin, DatabaseType type) {
        try {
            Class.forName(type.getDriver());
            if (type.equals(DatabaseType.SQLITE)) {
                this.loaded = true;
                File file = new File(plugin.getDataFolder(), plugin.getName().toLowerCase() + ".db");
                if (!file.exists()) file.createNewFile();
                return DriverManager.getConnection(type.getUrl() + file.getPath());
            }
            this.loaded = true;
            return DriverManager.getConnection(credentials.toUrl(type), credentials.getUsername(), credentials.getPassword());
        } catch (Exception ex) {
            SharkzLogger.error("&cCannot create database connection :");
            ex.printStackTrace();
        }
        return null;
    }

    public Connection getConnection() {
        return connection;
    }
}
