package me.sharkz.nexurereport.storage;

public interface DatabaseCredentials {

    String getHost();

    default int getPort() {
        return 3306;
    }

    String getPrefix();

    String getDatabase();

    String getUsername();

    String getPassword();

    default String toUrl(DatabaseType type) {
        return type.getUrl() + getHost() + ":" + getPort();
    }

}
