package me.sharkz.nexurereport.ui;

import me.sharkz.nexurereport.NR;
import me.sharkz.nexurereport.report.Report;
import me.sharkz.nexurereport.report.ReportsManager;
import me.sharkz.nexurereport.utils.items.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class AdminUI implements InventoryHolder, Listener {

    private Player player;
    private OfflinePlayer target;
    private final NR plugin;
    private final ReportsManager manager;

    /* Items */
    private ItemStack closeItem = new ItemBuilder(Material.RED_STAINED_GLASS_PANE).setName(ChatColor.RED + "Close").setLore("", ChatColor.AQUA + "!!" + ChatColor.GRAY + " Click to close this menu.").toItemStack();

    private final int[] OUTSIDE_SLOTS = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 48, 50, 51, 52, 53};
    private final int[] INSIDE_SLOTS = {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};

    public AdminUI() {
        this.plugin = NR.getPlugin(NR.class);
        this.manager = plugin.getReportsManager();
    }

    public AdminUI setTarget(OfflinePlayer target) {
        this.target = target;
        return this;
    }

    public void open(Player player) {
        this.player = player;
        Bukkit.getPluginManager().registerEvents(this, plugin);
        player.openInventory(getInventory());
    }

    @Override
    public Inventory getInventory() {
        Inventory inventory = Bukkit.createInventory(this, 54, ChatColor.GOLD + "Nexure Report " + ChatColor.GRAY + " | " + ChatColor.GREEN + " Admin");

        // Fillers
        for (int slot : OUTSIDE_SLOTS)
            inventory.setItem(slot, new ItemBuilder(Material.WHITE_STAINED_GLASS_PANE).setName(ChatColor.RESET + "").setLore(ChatColor.GRAY + "Developed by " + ChatColor.AQUA + "Sharkz").toItemStack());

        // Close btn
        inventory.setItem(49, closeItem);

        // Reports
        AtomicInteger i = new AtomicInteger();
        List<Report> reports = manager.getReports();
        if (target != null && target.hasPlayedBefore())
            reports = manager.get(target);
        Collections.reverse(reports);
        reports.stream()
                .filter(report -> report.getState().equals(Report.State.OPEN))
                .forEach(report -> {
                    if (i.get() < INSIDE_SLOTS.length) {
                        List<String> lore = new ArrayList<>();
                        lore.addAll(Arrays.asList("", "&7Message :"));
                        lore.addAll(splitLore(report.getContent(), 25));
                        lore.addAll(Arrays.asList("&7Reported by : &a&l" + report.getPlayer().getName(),
                                "&7Created on : &a&l" + new SimpleDateFormat("HH:mm EEE dd MMM yyyy").format(report.getDate())));
                        ItemStack head = new ItemBuilder(Material.PLAYER_HEAD)
                                .setName(ChatColor.AQUA + report.getTarget().getName())
                                .setLore(lore)
                                .toItemStack();
                        if (head.hasItemMeta()) {
                            SkullMeta meta = (SkullMeta) head.getItemMeta();
                            meta.setOwningPlayer(report.getTarget());
                            head.setItemMeta(meta);
                        }
                        inventory.setItem(INSIDE_SLOTS[i.get()], head);
                        i.getAndIncrement();
                    }
                });

        return inventory;
    }

    private ArrayList<String> splitLore(String text, int characters) {
        ArrayList<String> lore = new ArrayList<>();
        if (text.length() <= characters)
            lore.add(text);
        else {
            int beginIndex = 0;
            while (beginIndex <= text.length() - 1) {
                lore.add(text.substring(beginIndex, Math.min(beginIndex + characters, text.length())));
                beginIndex += characters;
            }
        }
        return lore;
    }

    @EventHandler
    public void onUIClick(InventoryClickEvent e) {
        if (e.getInventory().getHolder() == null
                || !e.getInventory().getHolder().equals(this)
                || !e.getWhoClicked().equals(player)
                || e.getCurrentItem() == null
                || e.getCurrentItem().getType().equals(Material.AIR)) return;
        e.setCancelled(true);
        if (e.getCurrentItem().equals(closeItem))
            player.closeInventory();
    }

    @EventHandler
    public void onUIDrag(InventoryDragEvent e) {
        if (e.getInventory().getHolder() != null
                && e.getInventory().getHolder().equals(this)
                && e.getWhoClicked().equals(player)) e.setCancelled(true);
    }

    @EventHandler
    public void onUIClose(InventoryCloseEvent e) {
        if (e.getInventory().getHolder() == null
                || !e.getInventory().getHolder().equals(this)
                || !e.getPlayer().equals(player)) return;
        InventoryCloseEvent.getHandlerList().unregister(this);
        InventoryDragEvent.getHandlerList().unregister(this);
        InventoryClickEvent.getHandlerList().unregister(this);
    }
}
