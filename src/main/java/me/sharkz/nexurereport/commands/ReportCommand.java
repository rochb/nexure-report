package me.sharkz.nexurereport.commands;

import me.sharkz.nexurereport.report.ReportsManager;
import me.sharkz.nexurereport.ui.AdminUI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ReportCommand implements CommandExecutor {

    private final ReportsManager reportsManager;

    public ReportCommand(ReportsManager reportsManager) {
        this.reportsManager = reportsManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command doesn't support console.");
            return true;
        }
        Player player = (Player) sender;
        if (args.length == 1) {
            if (!sender.hasPermission("nr.admin")) {
                sender.sendMessage(ChatColor.RED + "Wrong usage ! " + ChatColor.GREEN + " Try like this : /report <player> <reason>");
                return true;
            }
            OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
            if (target == null || !target.hasPlayedBefore()) {
                sender.sendMessage(ChatColor.RED + "Couldn't find this player.");
                return true;
            }

            new AdminUI()
                    .setTarget(target)
                    .open(player);
        } else if (args.length >= 2) {
            OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
            if (target == null || !target.hasPlayedBefore()) {
                sender.sendMessage(ChatColor.RED + "Couldn't find this player.");
                return true;
            }
            StringBuilder content = new StringBuilder();
            for (int i = 1; i < args.length; i++)
                content.append(args[i]).append(" ");
            reportsManager.create(player, target, content.toString());
            sender.sendMessage(ChatColor.GREEN + "Your report has been sent !");
        } else {
            if (!sender.hasPermission("nr.admin")) {
                sender.sendMessage(ChatColor.RED + "Wrong usage ! " + ChatColor.GREEN + " Try like this : /report <player> <reason>");
                return true;
            }
            new AdminUI()
                    .open(player);
        }
        return true;
    }
}
