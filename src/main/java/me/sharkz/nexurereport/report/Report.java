package me.sharkz.nexurereport.report;

import org.bukkit.OfflinePlayer;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public class Report implements Comparable<Report> {

    private final UUID id;
    private State state;
    private final OfflinePlayer player;
    private final OfflinePlayer target;
    private final String content;
    private final Date date;

    public Report(UUID id, State state, OfflinePlayer player, OfflinePlayer target, String content, Date date) {
        this.id = id;
        this.state = state;
        this.player = player;
        this.target = target;
        this.content = content;
        this.date = date;
    }

    public Report(OfflinePlayer player, OfflinePlayer target, String content) {
        this.id = UUID.randomUUID();
        this.date = new Date();
        this.state = State.OPEN;
        this.player = player;
        this.target = target;
        this.content = content;
    }

    public void setState(State state) {
        this.state = state;
    }

    public UUID getId() {
        return id;
    }

    public State getState() {
        return state;
    }

    public OfflinePlayer getPlayer() {
        return player;
    }

    public OfflinePlayer getTarget() {
        return target;
    }

    public String getContent() {
        return content;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public int compareTo(Report report) {
        if (getDate() == null || report.getDate() == null) return 0;
        return getDate().compareTo(report.getDate());
    }

    public enum State {
        OPEN(0),
        CLOSE(1);

        private final int id;

        State(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public static Optional<State> getById(int id) {
            return Arrays.stream(values()).filter(state1 -> state1.getId() == id).findFirst();
        }
    }
}
