package me.sharkz.nexurereport.report;

import me.sharkz.nexurereport.storage.DataManager;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ReportsManager {

    private final List<Report> reports = new ArrayList<>();
    private final DataManager dataManager;

    public ReportsManager(DataManager dataManager) {
        this.dataManager = dataManager;
        this.reports.addAll(dataManager.getAll());
    }

    public void create(Player player, OfflinePlayer target, String content) {
        Report report = new Report(player, target, content);
        dataManager.insert(report);
        reports.add(report);
    }

    public void setState(Report report, Report.State state) {
        if (report.getState().equals(state)) return;
        report.setState(state);
        dataManager.update(report);
    }

    public void delete(Report report) {
        if (!reports.contains(report)) return;
        reports.remove(report);
        dataManager.delete(report);
    }

    public Optional<Report> get(OfflinePlayer player, OfflinePlayer target) {
        return reports.stream().filter(report -> report.getPlayer().equals(player) && report.getTarget().equals(target)).findFirst();
    }

    public List<Report> get(OfflinePlayer player) {
        return reports.stream().filter(report -> report.getTarget().equals(player)).collect(Collectors.toList());
    }

    public List<Report> getSelf(OfflinePlayer player) {
        return reports.stream().filter(report -> report.getPlayer().equals(player)).collect(Collectors.toList());
    }

    public List<Report> getReports() {
        return reports;
    }
}
