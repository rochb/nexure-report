package me.sharkz.nexurereport;

import me.sharkz.nexurereport.commands.ReportCommand;
import me.sharkz.nexurereport.logger.SharkzLogger;
import me.sharkz.nexurereport.report.ReportsManager;
import me.sharkz.nexurereport.storage.DataManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class NR extends JavaPlugin {

    private DataManager dataManager;
    private ReportsManager reportsManager;

    public NR() {
        new SharkzLogger("&9&lNexure &b&lReport");
    }

    @Override
    public void onEnable() {
        /* Header */
        SharkzLogger.info("=================================");
        SharkzLogger.info("Version : " + getDescription().getVersion());
        SharkzLogger.info("Developed by : Sharkz");

        /* Configuration */
        saveDefaultConfig();

        /* Data */
        dataManager = new DataManager(this);

        /* Reports */
        reportsManager = new ReportsManager(dataManager);

        /* Commands */
        getCommand("report").setExecutor(new ReportCommand(reportsManager));

        /* Footer */
        SharkzLogger.info("=================================");
    }

    @Override
    public void onDisable() {
        /* Data */
        dataManager.getDatabase().closeConnection();

        /* Thanks */
        SharkzLogger.info("Nexure Report developed by Sharkz.");
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public ReportsManager getReportsManager() {
        return reportsManager;
    }
}
