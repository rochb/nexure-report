package me.sharkz.nexurereport.logger;

import org.bukkit.ChatColor;

public enum LogType {
    ERROR(ChatColor.RED),
    INFO(ChatColor.GRAY),
    WARNING(ChatColor.YELLOW),
    SUCCESS(ChatColor.GREEN);

    private final ChatColor color;

    LogType(ChatColor color) {
        this.color = color;
    }

    /**
     * Returns type's color
     * @return color
     */
    public ChatColor getColor() {
        return color;
    }
}
