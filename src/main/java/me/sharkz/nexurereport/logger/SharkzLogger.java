package me.sharkz.nexurereport.logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class SharkzLogger {

    private final String prefix;
    private static SharkzLogger instance;

    public SharkzLogger(String prefix) {
        this.prefix = prefix;
        instance = this;
    }

    /**
     * @param message
     */
    public static void error(String message) {
        log(message, LogType.ERROR);
    }

    /**
     * @param message
     */
    public static void warn(String message) {
        log(message, LogType.WARNING);
    }

    /**
     * @param message
     */
    public static void info(String message) {
        log(message, LogType.INFO);
    }

    /**
     * @param message
     */
    public static void success(String message) {
        log(message, LogType.SUCCESS);
    }

    /**
     * Send message to console
     *
     * @param message to send
     * @param type    to color
     */
    public static void log(String message, LogType type) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', getInstance().getPrefix() + " &r" + type.getColor() + message));
    }

    /**
     * Send message to console
     *
     * @param message to send
     */
    public static void log(String message) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', getInstance().getPrefix() + " &r" + message));
    }

    /**
     * Returns logger prefix
     *
     * @return logger prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Returns MilkaLogger instance
     *
     * @return instance
     */
    public static SharkzLogger getInstance() {
        return instance;
    }
}
