package me.sharkz.nexurereport.loader;

import org.bukkit.configuration.ConfigurationSection;

public interface Loader<T> {

    T load(ConfigurationSection configurationSection);

}
