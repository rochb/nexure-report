package me.sharkz.nexurereport.loader;

import me.sharkz.nexurereport.logger.SharkzLogger;
import me.sharkz.nexurereport.storage.DatabaseCredentials;
import org.bukkit.configuration.ConfigurationSection;

public class CredentialsLoader implements Loader<DatabaseCredentials> {

    @Override
    public DatabaseCredentials load(ConfigurationSection config) {
        if (!config.contains("host")
                || !config.contains("database")
                || !config.contains("username")
                || !config.contains("password")) {
            SharkzLogger.error("Invalid credentials configuration !");
            return null;
        }

        return new DatabaseCredentials() {
            @Override
            public String getHost() {
                return config.getString("host");
            }

            @Override
            public String getPrefix() {
                return config.getString("table-prefix", "nr");
            }

            @Override
            public String getDatabase() {
                return config.getString("database");
            }

            @Override
            public String getUsername() {
                return config.getString("username");
            }

            @Override
            public String getPassword() {
                return config.getString("password");
            }

            @Override
            public int getPort() {
                return config.getInt("port", 3306);
            }
        };
    }
}
